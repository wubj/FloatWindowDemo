package wubj.com.floatwindow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.bean.ItemBean;

/**
 * Wubj 创建于 2017/8/16.
 */
public class CustomAppAdapter extends BaseRecyclerAdapter<CustomAppAdapter.CustomAppHolder, ItemBean> {

    public CustomAppAdapter(List<ItemBean> mDataList) {
        super(mDataList);
    }

    @Override
    public CustomAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_custom_app_view, null);
        return new CustomAppHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomAppHolder holder, int position) {
        holder.bindView(position);
    }

    class CustomAppHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        CustomAppHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv_logo);
            textView = (TextView) view.findViewById(R.id.tv_name);
        }

        void bindView(final int position) {
            ItemBean item = mDataList.get(position);
            imageView.setImageResource(item.imgRes);
            textView.setText(item.name);
            if (onItemClickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(itemView, position);
                    }
                });
            }
        }
    }
}
