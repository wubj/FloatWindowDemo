package wubj.com.floatwindow.bean;

import java.io.Serializable;

/**
 * Wubj 创建于 2017/8/15.
 */
public class ItemBean implements Serializable {

    public String name;
    public String imgUrl;
    public int imgRes;
    public String packName;
    public String activityName;

    public ItemBean() {
    }

    public ItemBean(String name, int imgRes, String packName, String activityName) {
        this.name = name;
        this.imgRes = imgRes;
        this.packName = packName;
        this.activityName = activityName;
    }
}
