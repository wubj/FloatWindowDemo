package wubj.com.floatwindow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.bean.ItemBean;

/**
 * Wubj 创建于 2017/8/16.
 */
public class FloatAppAdapter extends BaseRecyclerAdapter<FloatAppAdapter.FloatAppHolder, ItemBean> {

    public FloatAppAdapter(List<ItemBean> mDataList) {
        super(mDataList);
    }

    @Override
    public FloatAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_float_view, null);
        return new FloatAppHolder(view);
    }

    @Override
    public void onBindViewHolder(FloatAppHolder holder, int position) {
        holder.bindView(position);
    }

    class FloatAppHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        FloatAppHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.iv_logo);
            textView = (TextView) view.findViewById(R.id.tv_name);
        }

        void bindView(final int position) {
            ItemBean item = mDataList.get(position);
            imageView.setImageResource(item.imgRes);
            textView.setText(item.name);
            if (onItemClickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(itemView, position);
                    }
                });
            }
        }
    }
}
