package wubj.com.floatwindow.bean;

import java.io.Serializable;

/**
 * Wubj 创建于 2017/8/16.
 */
public class WorkLogInfoBean implements Serializable {

    public String time;
    public String count;
    public String content;

    public WorkLogInfoBean(String time, String count, String content) {
        this.time = time;
        this.count = count;
        this.content = content;
    }
}
