package wubj.com.floatwindow.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import wubj.com.floatwindow.MyApplication;


/**
 * Wubj 创建于 2016/9/24.
 */
public class FloatPagerAdapter extends BasePagerAdapter<View> {

    public FloatPagerAdapter(ArrayList<View> mDataList) {
        super(mDataList);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(MyApplication.mContext);
//        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        //需要将imageView添加到container
        container.addView(imageView);
        return imageView;
    }
}
