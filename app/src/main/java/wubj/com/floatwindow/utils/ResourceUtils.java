package wubj.com.floatwindow.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import wubj.com.floatwindow.MyApplication;

/**
 * 通用工具
 * Created by wubaojie on 16/12/13.
 */
public class ResourceUtils {

    /**
     * 检查SD卡是否加载
     *
     * @return
     */
    public static boolean isSdcardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static Resources getResources() {
        return MyApplication.mContext.getResources();
    }

    /**
     * 获取resource下的图片
     *
     * @param id
     * @return
     */
    public static Drawable getDrawable(int id) {
        return MyApplication.mContext.getResources().getDrawable(id);
    }

    public static String getString(int id) {
        return MyApplication.mContext.getResources().getString(id);
    }

    public static String[] getStringArray(int id) {
        return MyApplication.mContext.getResources().getStringArray(id);
    }

    public static int getColor(int id) {
        return MyApplication.mContext.getResources().getColor(id);
    }

    /**
     * 获取dp资源，并且会自动将dp值转为px值
     *
     * @param id
     * @return
     */
    public static int getDimens(int id) {
        return MyApplication.mContext.getResources().getDimensionPixelSize(id);
    }
}
