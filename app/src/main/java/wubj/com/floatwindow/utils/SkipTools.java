package wubj.com.floatwindow.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Wubj 创建于 2017/8/10.
 */
public class SkipTools {

    public static final String PACKAGE_NAME = "cn.xspace.app";

    public static final String WORK_SIGN = "cn.xspace.module.mywork.activity.MyWorkSignActivity";
    public static final String WORK_LOG = "cn.xspace.module.mywork.activity.WorkLogMainActivity";
    public static final String WORK_TODO = "cn.xspace.module.mywork.activity.MyScheduleActivity";
    public static final String SERVICE_OBJ = "cn.xspace.wnx.activity.ServiceObjActivity";
    public static final String OFFLINE_DATA = "cn.xspace.module.mywork.activity.OfflineDataActivity";
    public static final String FORM_CQFY = "cn.xspace.module.form.activity.CQFYRecordListActivity";
    public static final String WNX_MAIN = "cn.xspace.wnx.activity.WnxMainActivity";
    public static final String QAC_MAIN = "cn.xspace.module.qac.activity.QacMainActivity";

    public static final String USER_CENTER = "cn.xspace.module.other.activity.SettingAct";
    public static final String USER_INFO = "cn.xspace.module.other.activity.UserInfoAct";

    public static void skip2Module(Context context, String packName, String activityName, String action) {
        Intent intent = getAppIntent(packName, activityName, null);
        intent.putExtra("action", action);
        context.startActivity(intent);
    }

    public static Intent getAppIntent(String packageName, String activityName, Bundle bundle) {
        ComponentName componentName = new ComponentName(packageName, activityName);
        Intent intent = new Intent();
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setComponent(componentName);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }
}
