package wubj.com.floatwindow.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.adapter.BaseRecyclerAdapter;
import wubj.com.floatwindow.adapter.FloatAppAdapter;
import wubj.com.floatwindow.bean.ItemBean;
import wubj.com.floatwindow.utils.ResourceUtils;
import wubj.com.floatwindow.utils.SkipTools;

/**
 * Wubj 创建于 2017/8/17.
 */
public class CustomAppView extends LinearLayout {

    private Context mContext;

    private static List<ItemBean> itemList = new ArrayList<>();

    static {
        String[] titles = ResourceUtils.getStringArray(R.array.float_group);
        int[] pics = {R.drawable.bg_mywork_sign_tab, R.drawable.bg_work_log, R.drawable.bg_work_todo,
                R.drawable.bg_work_object, R.drawable.bg_offline_data, R.drawable.bg_work_form,
                R.drawable.bg_wnx, R.drawable.bg_qac};
        String activityName[] = {SkipTools.WORK_SIGN, SkipTools.WORK_LOG, SkipTools.WORK_TODO, SkipTools.SERVICE_OBJ,
                SkipTools.OFFLINE_DATA, SkipTools.FORM_CQFY, SkipTools.WNX_MAIN, SkipTools.QAC_MAIN};
        for (int i = 0; i < titles.length; i++) {
            itemList.add(new ItemBean(titles[i], pics[i], SkipTools.PACKAGE_NAME, activityName[i]));
        }
    }


    public CustomAppView(Context context) {
        super(context);
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.custom_app_view, this);
        findView();
    }

    private void findView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_float_content);
        FloatAppAdapter floatAppAdapter = new FloatAppAdapter(itemList);
        floatAppAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                try {
                    ItemBean itemBean = itemList.get(position);
                    Intent intent = SkipTools.getAppIntent(itemBean.packName, itemBean.activityName, null);
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(floatAppAdapter);

    }
}
