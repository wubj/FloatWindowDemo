package wubj.com.floatwindow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.bean.WorkLogInfoBean;

/**
 * Wubj 创建于 2017/8/16.
 */
public class WorkLogAdapter extends BaseRecyclerAdapter<WorkLogAdapter.WorkLogHolder, WorkLogInfoBean> {

    public WorkLogAdapter(List<WorkLogInfoBean> mDataList) {
        super(mDataList);
    }

    @Override
    public WorkLogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_work_log_view, null);
        return new WorkLogHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkLogHolder holder, int position) {
        holder.bindView(position);
    }

    class WorkLogHolder extends RecyclerView.ViewHolder {

        TextView tvWorkLogTime;
        TextView tvWorkLogCount;
        TextView tvWorkLogContent;

        WorkLogHolder(View view) {
            super(view);
            tvWorkLogTime = (TextView) view.findViewById(R.id.tv_work_log_time);
            tvWorkLogCount = (TextView) view.findViewById(R.id.tv_work_log_count);
            tvWorkLogContent = (TextView) view.findViewById(R.id.tv_work_log_content);
        }

        void bindView(final int position) {
            WorkLogInfoBean item = mDataList.get(position);
            tvWorkLogTime.setText(item.time);
            tvWorkLogCount.setText(item.count);
            tvWorkLogContent.setText(item.content);
            if (onItemClickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(itemView, position);
                    }
                });
            }
        }
    }
}
