package wubj.com.floatwindow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * Wubj 创建于 2017/8/16.
 */
public abstract class BaseRecyclerAdapter<T extends RecyclerView.ViewHolder, W> extends RecyclerView.Adapter<T> {

    protected List<W> mDataList;

    protected OnItemClickListener onItemClickListener;
    protected OnItemLongClickListener onItemLongClickListener;

    public BaseRecyclerAdapter(List<W> mDataList) {
        this.mDataList = mDataList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

}
