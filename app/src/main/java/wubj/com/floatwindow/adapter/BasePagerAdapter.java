package wubj.com.floatwindow.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Wubj on 2016/9/25.
 * ViewPager的基类,封装了公共的方法
 */
public class BasePagerAdapter<T> extends PagerAdapter {

    protected ArrayList<T> mDataList;

    public BasePagerAdapter(ArrayList<T> mDataList) {
        super();
        this.mDataList = mDataList;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
