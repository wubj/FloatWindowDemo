package wubj.com.floatwindow.utils;

import android.content.Context;
import android.widget.Toast;

import wubj.com.floatwindow.MyApplication;

//Toast统一管理类
public class ToastU {


    private static Toast sToast;

    private ToastU() {
        /* cannot be instantiated */
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean isShow = true;


    /**
     * 长时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showLong(Context context, CharSequence message) {
        if (isShow)
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * 长时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showLong(Context context, int message) {
        if (isShow)
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * 自定义显示Toast时间
     *
     * @param context
     * @param message
     * @param duration
     */
    public static void show(Context context, CharSequence message, int duration) {
        if (isShow)
            Toast.makeText(context, message, duration).show();
    }

    /**
     * 自定义显示Toast时间
     *
     * @param context
     * @param message
     * @param duration
     */
    public static void show(Context context, int message, int duration) {
        if (isShow)
            Toast.makeText(context, message, duration).show();
    }

    /**
     * 显示一个指定时长的单例吐司
     *
     * @param msg      消息
     * @param duration 时长
     */
    public static void showSingle(String msg, int duration) {
        if (sToast == null) {
            sToast = Toast.makeText(MyApplication.mContext, msg, duration);
        }
        sToast.setText(msg);
        sToast.setDuration(duration);
        sToast.show();
    }

    /**
     * 输出一个单例吐司,显示较短时长
     *
     * @param text 要显示的文本
     */
    public static void showSingleS(String text) {
        showSingle(text, Toast.LENGTH_SHORT);
    }

}