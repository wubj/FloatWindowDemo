package wubj.com.floatwindow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.bean.WorkSignBean;

/**
 * Wubj 创建于 2017/8/16.
 */
public class WorkSignAdapter extends BaseRecyclerAdapter<WorkSignAdapter.WorkSignHolder, WorkSignBean> {

    public WorkSignAdapter(List<WorkSignBean> mDataList) {
        super(mDataList);
    }

    @Override
    public WorkSignHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_work_sign_view, null);
        return new WorkSignHolder(view);
    }

    @Override
    public void onBindViewHolder(WorkSignHolder holder, int position) {
        holder.bindView(position);
    }

    class WorkSignHolder extends RecyclerView.ViewHolder {

        TextView tvWorkSignTime;
        TextView tvWorkSignType;
        TextView tvWorkSignDate;

        WorkSignHolder(View view) {
            super(view);
            tvWorkSignTime = (TextView) view.findViewById(R.id.tv_work_sign_time);
            tvWorkSignType = (TextView) view.findViewById(R.id.tv_work_sign_type);
            tvWorkSignDate = (TextView) view.findViewById(R.id.tv_work_sign_date);
        }

        void bindView(final int position) {
            WorkSignBean item = mDataList.get(position);
            tvWorkSignTime.setText(item.time);
            tvWorkSignType.setText(item.type);
            tvWorkSignDate.setText(item.date);
            if (onItemClickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(itemView, position);
                    }
                });
            }
        }
    }
}
