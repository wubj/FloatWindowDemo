package wubj.com.floatwindow.bean;

import java.io.Serializable;

/**
 * Wubj 创建于 2017/8/16.
 */
public class WorkSignBean implements Serializable {

    public String time;
    public String type;
    public String date;

    public WorkSignBean(String time, String type, String date) {
        this.time = time;
        this.type = type;
        this.date = date;
    }
}
