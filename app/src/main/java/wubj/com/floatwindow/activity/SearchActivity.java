package wubj.com.floatwindow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import wubj.com.floatwindow.R;
import wubj.com.floatwindow.adapter.BaseRecyclerAdapter;
import wubj.com.floatwindow.adapter.CustomAppAdapter;
import wubj.com.floatwindow.adapter.WorkLogAdapter;
import wubj.com.floatwindow.adapter.WorkSignAdapter;
import wubj.com.floatwindow.bean.ItemBean;
import wubj.com.floatwindow.bean.WorkLogInfoBean;
import wubj.com.floatwindow.bean.WorkSignBean;
import wubj.com.floatwindow.utils.ResourceUtils;
import wubj.com.floatwindow.utils.SkipTools;

public class SearchActivity extends AppCompatActivity {

    private Context mContext;
    private EditText mEditTopSearch;
    private RelativeLayout mRlTopSearch;
    private RecyclerView mRecyclerCustomApp;
    private RecyclerView mRecyclerWorkLog;
    private TextView mTvWorkLogViewMore;
    private RecyclerView mRecyclerWorkSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_search);
        initView();
        initData();
        initAdapter();
    }

    private void initView() {
        mEditTopSearch = (EditText) findViewById(R.id.edit_top_search);
        mRlTopSearch = (RelativeLayout) findViewById(R.id.rl_top_search);
        mRecyclerCustomApp = (RecyclerView) findViewById(R.id.recycler_custom_app);
        mRecyclerWorkLog = (RecyclerView) findViewById(R.id.recycler_work_log);
        mTvWorkLogViewMore = (TextView) findViewById(R.id.tv_work_log_view_more);
        mRecyclerWorkSign = (RecyclerView) findViewById(R.id.recycler_work_sign);
        mTvWorkLogViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoWorkLog();
            }
        });
        mEditTopSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //当actionId == XX_SEND 或者 XX_DONE时都触发
                //或者event.getKeyCode == ENTER 且 event.getAction == ACTION_DOWN时也触发
                //注意，这是一定要判断event != null。因为在某些输入法上会返回null。
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_SEND
                        || actionId == EditorInfo.IME_ACTION_DONE
                        /*|| (event != null && KeyEvent.KEYCODE_ENTER == event.getKeyCode() && KeyEvent.ACTION_DOWN ==
                        event.getAction())*/) {
                    //处理事件
                    submit();
                }
                return false;
            }
        });
    }

    private List<ItemBean> customApp = new ArrayList<>();
    private List<WorkLogInfoBean> workLog = new ArrayList<>();
    private List<WorkSignBean> WorkSign = new ArrayList<>();

    private void initData() {
        initCustomData();
        initWorkLogData();
        initWorkSignData();
    }

    private void initWorkSignData() {
        String content = " 电脑端 ip : 127.0.0.1";
        for (int i = 0; i < 4; i++) {
            WorkSign.add(new WorkSignBean("12:5" + i, "签到", "06/2" + i + content));
        }
    }

    private void initWorkLogData() {
        String content = "今天工作内容主要是进行了这个界面的设计,并且对其中的做了处理";
        for (int i = 0; i < 2; i++) {
            workLog.add(new WorkLogInfoBean("2017/8/16 " + i + "2:00", "回复数:" + i, content));
        }
    }

    private void initCustomData() {
        String[] titles = ResourceUtils.getStringArray(R.array.float_group);
        int[] pics = {R.drawable.bg_mywork_sign_tab, R.drawable.bg_work_log, R.drawable.bg_work_todo,
                R.drawable.bg_work_object, R.drawable.bg_offline_data, R.drawable.bg_work_form,
                R.drawable.bg_wnx, R.drawable.bg_qac};
        String activityName[] = {SkipTools.WORK_SIGN, SkipTools.WORK_LOG, SkipTools.WORK_TODO, SkipTools.SERVICE_OBJ,
                SkipTools.OFFLINE_DATA, SkipTools.FORM_CQFY, SkipTools.WNX_MAIN, SkipTools.QAC_MAIN};
        for (int i = 0; i < titles.length; i++) {
            customApp.add(new ItemBean(titles[i], pics[i], SkipTools.PACKAGE_NAME, activityName[i]));
        }
    }

    private void initAdapter() {
        initCustomAppAdapter();
        initWorkLogAdapter();
        initWorkSignAdapter();
    }

    private void initWorkSignAdapter() {
        WorkSignAdapter workSignAdapter = new WorkSignAdapter(WorkSign);
        workSignAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                startActivity(SkipTools.getAppIntent(SkipTools.PACKAGE_NAME, SkipTools.WORK_SIGN, null));
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerWorkSign.setLayoutManager(gridLayoutManager);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mRecyclerWorkSign.setLayoutManager(linearLayoutManager);
        mRecyclerWorkSign.setAdapter(workSignAdapter);
    }

    private void initWorkLogAdapter() {
        WorkLogAdapter workLogAdapter = new WorkLogAdapter(workLog);
        workLogAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gotoWorkLog();
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerWorkLog.setLayoutManager(gridLayoutManager);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mRecyclerWorkLog.setLayoutManager(linearLayoutManager);
        mRecyclerWorkLog.setAdapter(workLogAdapter);
    }

    private void gotoWorkLog() {
        startActivity(SkipTools.getAppIntent(SkipTools.PACKAGE_NAME, SkipTools.WORK_LOG, null));
    }

    private void initCustomAppAdapter() {
        CustomAppAdapter customAppAdapter = new CustomAppAdapter(customApp);
        customAppAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ItemBean itemBean = customApp.get(position);
                Intent intent = SkipTools.getAppIntent(itemBean.packName, itemBean.activityName, null);
                startActivity(intent);
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerCustomApp.setLayoutManager(gridLayoutManager);
        mRecyclerCustomApp.setAdapter(customAppAdapter);
    }

    private void submit() {
        // validate
        String search = mEditTopSearch.getText().toString().trim();
        if (TextUtils.isEmpty(search)) {
            Toast.makeText(this, "搜索应用、图标、新闻、资讯……", Toast.LENGTH_SHORT).show();
            return;
        }
    }
}