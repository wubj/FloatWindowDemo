package wubj.com.floatwindow.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wubj.com.floatwindow.FloatWindowService;
import wubj.com.floatwindow.MyWindowManager;
import wubj.com.floatwindow.R;
import wubj.com.floatwindow.adapter.BaseRecyclerAdapter;
import wubj.com.floatwindow.adapter.FloatAppAdapter;
import wubj.com.floatwindow.bean.ItemBean;
import wubj.com.floatwindow.utils.ResourceUtils;
import wubj.com.floatwindow.utils.SkipTools;

/**
 * Wubj 创建于 2017/8/14.
 */
public class FloatWindowBigView extends LinearLayout {

    public static int viewWidth;
    public static int viewHeight;
    private Context mContext;

    private static List<ItemBean> itemList = new ArrayList<>();

    static {
        String[] titles = ResourceUtils.getStringArray(R.array.float_group);
        int[] pics = {R.drawable.bg_mywork_sign_tab, R.drawable.bg_work_log, R.drawable.bg_work_todo,
                R.drawable.bg_work_object, R.drawable.bg_offline_data, R.drawable.bg_work_form,
                R.drawable.bg_wnx, R.drawable.bg_qac};
        String activityName[] = {SkipTools.WORK_SIGN, SkipTools.WORK_LOG, SkipTools.WORK_TODO, SkipTools.SERVICE_OBJ,
                SkipTools.OFFLINE_DATA, SkipTools.FORM_CQFY, SkipTools.WNX_MAIN, SkipTools.QAC_MAIN};
        for (int i = 0; i < titles.length; i++) {
            itemList.add(new ItemBean(titles[i], pics[i], SkipTools.PACKAGE_NAME, activityName[i]));
        }
    }

    public FloatWindowBigView(final Context context) {
        super(context);
        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.float_window_big, this);
//        ViewPager container = (ViewPager) findViewById(R.id.vp_container);
//        PagerAdapter mainPagerAdapter = new FloatPagerAdapter(getContext().getSupportFragmentManager());
//        container.setAdapter(mainPagerAdapter);
        findView();
        View view = findViewById(R.id.big_window_layout);
        viewWidth = view.getLayoutParams().width;
        viewHeight = view.getLayoutParams().height;
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hiddenBig();
            }
        });
    }

    private void findView() {
        try {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_float_content);
            FloatAppAdapter floatAppAdapter = new FloatAppAdapter(itemList);
            floatAppAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    hiddenBig();
                    try {
                        ItemBean itemBean = itemList.get(position);
                        Intent intent = SkipTools.getAppIntent(itemBean.packName, itemBean.activityName, null);
                        mContext.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
            gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(floatAppAdapter);

//            ItemAdapter adapter = new ItemAdapter();
//            gridView.setAdapter(adapter);
//            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                    if (mListener != null) {
////                        mListener.clickListener(parent, view, position, id);
////                    }
//                }
//            });
            Button close = (Button) findViewById(R.id.bt_close);
            Button back = (Button) findViewById(R.id.bt_back);
            close.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 点击关闭悬浮窗的时候，移除所有悬浮窗，并停止Service
                    MyWindowManager.removeBigWindow(mContext);
                    MyWindowManager.removeSmallWindow(mContext);
                    Intent intent = new Intent(getContext(), FloatWindowService.class);
                    mContext.stopService(intent);
                }
            });
            back.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 点击返回的时候，移除大悬浮窗，创建小悬浮窗
                    hiddenBig();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hiddenBig() {
        MyWindowManager.removeBigWindow(mContext);
        MyWindowManager.createSmallWindow(mContext);
    }

    private class ItemAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return itemList.size();
        }

        @Override
        public ItemBean getItem(int position) {
            return itemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = viewHolder.holderView;
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.bindData(getItem(position));
            return convertView;
        }
    }

    private class ViewHolder {
        View holderView;
        ImageView imageView;
        TextView textView;

        ViewHolder() {
            holderView = initHolderView();
            holderView.setTag(this);
        }

        View initHolderView() {
            View view = View.inflate(mContext, R.layout.item_float_view, null);
            imageView = (ImageView) view.findViewById(R.id.iv_logo);
            textView = (TextView) view.findViewById(R.id.tv_name);
            return view;
        }

        void bindData(ItemBean item) {
            imageView.setImageResource(item.imgRes);
            textView.setText(item.name);
        }
    }
}
