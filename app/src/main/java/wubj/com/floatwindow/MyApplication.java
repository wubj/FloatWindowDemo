package wubj.com.floatwindow;

import android.app.Application;
import android.content.Context;

/**
 * Wubj 创建于 2017/8/15.
 */
public class MyApplication extends Application {

    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }
}
